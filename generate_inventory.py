import json
import sys

def generate_ansible_inventory(input_file):
    with open(input_file, 'r') as f:
        data = json.load(f)

    inventory = {
        'app': [],
        'balancer': [],
        'db': [],
        'prod': {
            'children': ['app', 'balancer', 'db']
        }
    }

    for key, value in data['vm_info']['value'].items():
        group_name = key.replace('prod01-', '')
        inventory[group_name].append(f"{key} ansible_host={value['ip']}")

    with open("hosts", 'w') as f:
        for group, hosts in inventory.items():
            if group == 'prod':
                f.write(f"[{group}:children]\n")
                for child_group in hosts['children']:
                    f.write(f"{child_group}\n")
                f.write('\n')
            else:
                f.write(f"[{group}]\n")
                for host in hosts:
                    f.write(f"{host}\n")
                f.write('\n')

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python script.py input_file")
        sys.exit(1)

    input_file = sys.argv[1]

    generate_ansible_inventory(input_file)